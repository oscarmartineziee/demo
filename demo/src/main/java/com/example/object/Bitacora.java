package com.example.object;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
 
import com.vladmihalcea.hibernate.type.json.JsonStringType;

@TypeDef(
	    name = "json",
	    typeClass = JsonStringType.class
	)
	 
@Entity // This tells Hibernate to make a table out of this class
@Where(clause="deleted_at is null")
public class Bitacora {

	    @Id
	    @GeneratedValue(strategy=GenerationType.AUTO)
	    private Integer id;
	    @Type( type = "json" )
	    @Column( columnDefinition = "json" )
	    private String data_new;
	    @Type( type = "json" )
	    @Column( columnDefinition = "json" )
	    private String data_old;
	    @Type( type = "json" )
	    @Column( columnDefinition = "json" )
	    private String changes;
	    private String event;
	    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	    @ManyToOne(fetch = FetchType.LAZY,
	            cascade = {
	                CascadeType.PERSIST,
	                CascadeType.MERGE
	            })
	    private User userId;
	    private Date createdAt;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getData_new() {
			return data_new;
		}
		public void setData_new(String data_new) {
			this.data_new = data_new;
		}
		public String getData_old() {
			return data_old;
		}
		public void setData_old(String data_old) {
			this.data_old = data_old;
		}
		public String getChanges() {
			return changes;
		}
		public void setChanges(String changes) {
			this.changes = changes;
		}
		public String getEvent() {
			return event;
		}
		public void setEvent(String event) {
			this.event = event;
		}
		public User getUserId() {
			return userId;
		}
		public void setUserId(User userId) {
			this.userId = userId;
		}
		public Date getCreatedAt() {
			return createdAt;
		}
		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}
	    
	    
}
