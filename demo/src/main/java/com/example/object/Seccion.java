package com.example.object;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Subselect;
@Entity 
@Subselect("select * from seccion")
public class Seccion {

	 
// This tells Hibernate to make a table out of this class
 
	    @Id
	    @GeneratedValue(strategy=GenerationType.AUTO)
	    private Integer id;
	    private String municipio;
	    private String cabecera;
	    private String distrito;
	    private String cabecera2;
	    private String distrito2;
	    private String seccion;
	    private String municipio2;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getMunicipio() {
			return municipio;
		}
		public void setMunicipio(String municipio) {
			this.municipio = municipio;
		}
		public String getCabecera() {
			return cabecera;
		}
		public void setCabecera(String cabecera) {
			this.cabecera = cabecera;
		}
		public String getDistrito() {
			return distrito;
		}
		public void setDistrito(String distrito) {
			this.distrito = distrito;
		}
		public String getCabecera2() {
			return cabecera2;
		}
		public void setCabecera2(String cabecera2) {
			this.cabecera2 = cabecera2;
		}
		public String getDistrito2() {
			return distrito2;
		}
		public void setDistrito2(String distrito2) {
			this.distrito2 = distrito2;
		}
		public String getSeccion() {
			return seccion;
		}
		public void setSeccion(String seccion) {
			this.seccion = seccion;
		}
		public String getMunicipio2() {
			return municipio2;
		}
		public void setMunicipio2(String municipio2) {
			this.municipio2 = municipio2;
		}
	    
	    

}
