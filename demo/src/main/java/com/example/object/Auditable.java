package com.example.object;

import java.sql.Timestamp;

public interface Auditable {

    Timestamp getCreatedAt();

    void setCreatedAt(Timestamp dateCreated);

    Timestamp getUpdatedAt();

    void setUpdatedAt(Timestamp lastUpdated);
}