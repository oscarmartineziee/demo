package com.example.object;
 
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
@TypeDef(
	    name = "json",
	    typeClass = JsonStringType.class
	)
	 
@Entity 
public class BitacoraProcesos {
		    @Id
		    @GeneratedValue(strategy = GenerationType.IDENTITY)
		    private Integer id;
		    private String uri;
		    @Type( type = "json" )
		    @Column( columnDefinition = "json" )
		    private String data_request;
		    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
		    @ManyToOne(fetch = FetchType.LAZY)
		    private User user;
		    private Timestamp createdAt;
		    private String mac;
		    private String ip;
		    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
		    @ManyToOne(fetch = FetchType.LAZY)
		    private MatrizSeguridad matrizSeguridad;
			public Integer getId() {
				return id;
			}
			public void setId(Integer id) {
				this.id = id;
			}
			public String getUri() {
				return uri;
			}
			public void setUri(String uri) {
				this.uri = uri;
			}
			public String getData_request() {
				return data_request;
			}
			public void setData_request(String data_request) {
				this.data_request = data_request;
			}
			public User getUser() {
				return user;
			}
			public void setUser(User user) {
				this.user = user;
			}
			public Timestamp getCreatedAt() {
				return createdAt;
			}
			public void setCreatedAt(Timestamp createdAt) {
				this.createdAt = createdAt;
			}
			public String getMac() {
				return mac;
			}
			public void setMac(String mac) {
				this.mac = mac;
			}
			public String getIp() {
				return ip;
			}
			public void setIp(String ip) {
				this.ip = ip;
			}
			public MatrizSeguridad getMatrizSeguridad() {
				return matrizSeguridad;
			}
			public void setMatrizSeguridad(MatrizSeguridad matrizSeguridad) {
				this.matrizSeguridad = matrizSeguridad;
			}
		    
		    
}