package com.example.object;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity // This tells Hibernate to make a table out of this class
@Where(clause="deleted_at is null")
public class ConsejoUsuarios {

 
	    @Id
	    @GeneratedValue(strategy=GenerationType.AUTO)
	    private Integer id;
	    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	    @ManyToOne(fetch = FetchType.EAGER,
	            cascade = {
	                CascadeType.PERSIST,
	                CascadeType.MERGE
	            })
	    private User user;
	    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	    @ManyToOne(fetch = FetchType.EAGER,
	            cascade = {
	                CascadeType.PERSIST,
	                CascadeType.MERGE
	            })
	    private Consejos consejo;
	    private Date createdAt;
	    private Date updatedAt;
	    private Date deletedAt;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		public Consejos getConsejo() {
			return consejo;
		}
		public void setConsejo(Consejos consejo) {
			this.consejo = consejo;
		}
		public Date getCreated_at() {
			return createdAt;
		}
		public void setCreated_at(Date created_at) {
			this.createdAt = created_at;
		}
		public Date getUpdated_at() {
			return updatedAt;
		}
		public void setUpdated_at(Date updated_at) {
			this.updatedAt = updated_at;
		}
		public Date getDeleted_at() {
			return deletedAt;
		}
		public void setDeleted_at(Date deleted_at) {
			this.deletedAt = deleted_at;
		}
	    
}
