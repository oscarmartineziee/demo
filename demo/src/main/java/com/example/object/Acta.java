package com.example.object;
 
import java.sql.Timestamp;
import java.util.Arrays;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
 

 
 
@Entity // This tells Hibernate to make a table out of this class
@EntityListeners(AuditableListener.class)
public class Acta  implements Auditable {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Integer id;
	    private String url;
	    private String qr;
	    private String folio;
	    private String tipoEleccion;
	    private String tipoActa;
	    private String seccion;
	    private String casilla;
	    private String distrito;
	    private String fecha;
	    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	    @ManyToOne(fetch = FetchType.EAGER)
	    private User user;
	    @Column(columnDefinition="LONGTEXT")
	    private String baseByte;
	    @Column(columnDefinition="LONGBLOB")
	    private  byte[]  img;
	    
	    private Timestamp  createdAt;
	    private Timestamp  updatedAt;
	    private Timestamp  deletedAt;
	    
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getFecha() {
			return fecha;
		}
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		public byte[] getImg() {
			return img;
		}
		public void setImg(byte[] img) {
			this.img = img;
		}
		
		public String getBaseByte() {
			return baseByte;
		}
		public void setBaseByte(String baseByte) {
			this.baseByte = baseByte;
		}
		
		public String getQr() {
			return qr;
		}
		public void setQr(String qr) {
			this.qr = qr;
		}
		
		
		public String getFolio() {
			return folio;
		}
		public void setFolio(String folio) {
			this.folio = folio;
		}
		public String getTipoEleccion() {
			return tipoEleccion;
		}
		public void setTipoEleccion(String tipoEleccion) {
			this.tipoEleccion = tipoEleccion;
		}
		public String getTipoActa() {
			return tipoActa;
		}
		public void setTipoActa(String tipoActa) {
			this.tipoActa = tipoActa;
		}
		public String getSeccion() {
			return seccion;
		}
		public void setSeccion(String seccion) {
			this.seccion = seccion;
		}
		public String getCasilla() {
			return casilla;
		}
		public void setCasilla(String casilla) {
			this.casilla = casilla;
		}
		
		public String getDistrito() {
			return distrito;
		}
		public void setDistrito(String distrito) {
			this.distrito = distrito;
		}
		@Override
		public String toString() {
			return "Acta [id=" + id + ", url=" + url + ", fecha=" + fecha + ", img=" + Arrays.toString(img) + "]";
		}
		public User getUser() {
			return user;
		}
		public void setUser(User user) {
			this.user = user;
		}
		 @Override
		public Timestamp getCreatedAt() {
			return createdAt;
		}
		 @Override
		public void setCreatedAt(Timestamp createdAt) {
			this.createdAt = createdAt;
		}
		 @Override
		public Timestamp getUpdatedAt() {
			return updatedAt;
		}
		 @Override
		public void setUpdatedAt(Timestamp updatedAt) {
			this.updatedAt = updatedAt;
		}
		public Timestamp getDeletedAt() {
			return deletedAt;
		}
		public void setDeletedAt(Timestamp deletedAt) {
			this.deletedAt = deletedAt;
		}
	    
		
	    

}
