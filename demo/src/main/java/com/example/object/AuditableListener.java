package com.example.object;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.sql.Timestamp;
import java.time.Instant;

public class AuditableListener {
    @PrePersist
    void preCreate(Auditable auditable) {
        Timestamp now = Timestamp.from(Instant.now());
        auditable.setCreatedAt(now);
        auditable.setUpdatedAt(now);
    }

    @PreUpdate
    void preUpdate(Auditable auditable) {
        Timestamp now = Timestamp.from(Instant.now());
        auditable.setUpdatedAt(now);
    }
}