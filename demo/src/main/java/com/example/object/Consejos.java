package com.example.object;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Where;

@Entity // This tells Hibernate to make a table out of this class
@Where(clause="deleted_at is null")
public class Consejos {
	    @Id
	    @GeneratedValue(strategy=GenerationType.AUTO)
	    private Integer id;
	    private String nombre;
	    private String ubicacion;
	    private Date createdAt;
	    private Date updatedAt;
	    private Date deletedAt;
	    
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getUbicacion() {
			return ubicacion;
		}
		public void setUbicacion(String ubicacion) {
			this.ubicacion = ubicacion;
		}
		public Date getCreated_at() {
			return createdAt;
		}
		public void setCreated_at(Date created_at) {
			this.createdAt = created_at;
		}
		public Date getUpdated_at() {
			return updatedAt;
		}
		public void setUpdated_at(Date updated_at) {
			this.updatedAt = updated_at;
		}
		public Date getDeleted_at() {
			return deletedAt;
		}
		public void setDeleted_at(Date deleted_at) {
			this.deletedAt = deleted_at;
		}
	    
	    
	    
}
