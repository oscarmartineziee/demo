package com.example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.object.Seccion;

public interface SeccionRepository extends CrudRepository<Seccion, Integer> {
 
	Seccion findBySeccion(String seccion);
}

