package com.example.repositories;

import org.springframework.data.repository.CrudRepository;
 
import com.example.object.ConsejoUsuarios;
 
 

public interface ConsejoUsuarioRepository extends CrudRepository<ConsejoUsuarios, Integer> {

   ConsejoUsuarios findByConsejoIdAndUserId(Integer consejo, Integer user);
   ConsejoUsuarios findByUserId(Integer user);
}
