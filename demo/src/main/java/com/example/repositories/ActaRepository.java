package com.example.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.object.Acta;
@Repository
public interface ActaRepository extends CrudRepository<Acta, Integer> {

	 Acta findTopByOrderByIdDesc();
	 Acta findTopByOrderByIdAsc();
}

