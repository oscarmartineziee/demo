package com.example.repositories;

import org.springframework.data.repository.CrudRepository;
 
import com.example.object.Equipos;

 
public interface EquipoRepository extends CrudRepository<Equipos, Integer> {

//No property deletedAt found for type Equipos! Did you mean 'deleted_at'?
	Equipos findByMacAndIp(String mac, String ip);
}
