package com.example.repositories;

import org.springframework.data.repository.CrudRepository;
 
import com.example.object.Consejos;

public interface ConsejoRepository extends CrudRepository<Consejos, Integer> {

	Consejos findTopByOrderByIdDesc();
	Consejos findTopByOrderByIdAsc();
}

