package com.example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.object.Perfiles;
 
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PerfilRepository extends CrudRepository<Perfiles ,Integer> {
 
}