package com.example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.object.MatrizSeguridad;

 
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface MatrizSeguridadRepository extends CrudRepository<MatrizSeguridad, Integer> {
	
	MatrizSeguridad findByProcesoUriAndPerfilId(String proceso, Integer id);
	MatrizSeguridad findById(int i);
}