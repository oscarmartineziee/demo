package com.example.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.object.BitacoraProcesos;
@Repository
public interface BitacoraProcesosRespository extends CrudRepository<BitacoraProcesos, Integer> {


}


