package com.example.demo;


import com.example.object.Consejos;
import com.example.object.Equipos;
import com.example.object.Perfiles;
import com.example.object.User;
import com.google.gson.Gson;
  
public class HeaderIEE {

 
	private User usuario;
	private Consejos consejo;
	private Perfiles perfil;
	private Equipos equipo;
	
	 
	public User getUsuario() {
		return usuario;
	}
	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}
	public Consejos getConsejo() {
		return consejo;
	}
	public void setConsejo(Consejos consejo) {
		this.consejo = consejo;
	}
	public Perfiles getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfiles perfil) {
		this.perfil = perfil;
	}
	public Equipos getEquipo() {
		return equipo;
	}
	public void setEquipo(Equipos equipo) {
		this.equipo = equipo;
	}
	public HeaderIEE decodeHeader(String header)
	{
		byte[] bytes =null;
		bytes=  java.util.Base64.getDecoder().decode(header);
		Gson s= new Gson();
		return s.fromJson( new String(bytes), HeaderIEE.class);
	
	}
	@Override
	public String toString() {
		  return new com.google.gson.Gson().toJson(this);
	}
	
	
	
}
