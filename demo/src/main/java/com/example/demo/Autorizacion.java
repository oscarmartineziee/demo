package com.example.demo;
 
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
 
 

public class Autorizacion {
	
	public String getHeaderValue( HttpServletRequest request,String key ) {
		 
		Map<String, String> result = new HashMap<>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			 String pkey = (String) headerNames.nextElement();
			 String value = request.getHeader(pkey);
			result.put(pkey, value);
			
			if (pkey.equals(key))
			{
				//System.out.println(pkey + ":" + value);
				 return value;
			}
 
		} 
			return "";
	}
}
