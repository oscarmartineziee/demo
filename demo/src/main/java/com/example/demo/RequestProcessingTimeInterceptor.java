package com.example.demo;

import java.sql.Timestamp;
import java.time.Instant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.example.object.BitacoraProcesos;
import com.example.object.MatrizSeguridad;
import com.example.repositories.BitacoraProcesosRespository;
import com.example.repositories.MatrizSeguridadRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
@Component
public class RequestProcessingTimeInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private MatrizSeguridadRepository matrizRepository;
	private static final Logger logger = LoggerFactory
			.getLogger(RequestProcessingTimeInterceptor.class);

	@Autowired
	private BitacoraProcesosRespository bitProcesos;
	Autorizacion headerReader= new Autorizacion();
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.info("preHandle: Request URL::" + request.getRequestURL().toString());
		int perfil=0;
		boolean respuesta=false;
		MatrizSeguridad tmp = null;
		
		String[] uri= request.getRequestURI().split("/");
		String rq= uri[uri.length-1];
		if(rq.equals("imageAndData"))return true;
		String vieneDeApp=headerReader.getHeaderValue(request, "Expect");
		String data=headerReader.getHeaderValue(request, "xData");
		perfil=Integer.parseInt(headerReader.getHeaderValue(request, "xEncodePf"));
		if(vieneDeApp.equals("100-continue"))
			respuesta= true;
		if(perfil!=0)
		{
			tmp= matrizRepository.findByProcesoUriAndPerfilId(rq,perfil);
			logger.info("Petición: "+rq+"; de: " +request.getRemoteAddr());
			if(tmp!=null)
				respuesta=true;
			else
				respuesta=false;
		}
		else 
			respuesta=false;
		if(respuesta)
			logger.info("Autorizado: Request URL::" + request.getRequestURL().toString()+ ":: de:"+request.getRemoteAddr());
		else
			logger.info("Negado: Request URL::" + request.getRequestURL().toString()+ ":: de:"+request.getRemoteAddr());
		
		 bitacora(  rq,   data,  tmp,   request);
	
		return respuesta;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//System.out.println("postHandle: Request URL::" + request.getRequestURL().toString()
		//		+ " Sent to Handler :: Current Time=" + System.currentTimeMillis());
		//we can add attributes in the modelAndView and use that in the view page
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		//long startTime = (Long) request.getAttribute("startTime");
		//logger.info("afterCompletion: Request URL::" + request.getRequestURL().toString()
			//	+ ":: End Time=" + System.currentTimeMillis());
	//	logger.info("Request URL::" + request.getRequestURL().toString()
		//		+ ":: Time Taken=" + (System.currentTimeMillis() - startTime));
	}

	private void bitacora(String rq, String data, MatrizSeguridad matriz,  HttpServletRequest request)
	{
		BitacoraProcesos bitP= new BitacoraProcesos();
		bitP.setUri(rq);
		Gson g = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		bitP.setData_request(data);
		Timestamp now = Timestamp.from(Instant.now());
		String headerc=headerReader.getHeaderValue(request, "headerIEE");
		HeaderIEE header= g.fromJson(headerc, HeaderIEE.class);
		
		bitP.setCreatedAt(now);
		if(header.getEquipo()!=null)
		bitP.setMac(header.getEquipo().getMac());
		else
			bitP.setMac("");
		bitP.setIp(request.getRemoteAddr());
		bitP.setMatrizSeguridad(matriz);
		bitP.setUser(header.getUsuario());
		bitProcesos.save(bitP);
	}
}
