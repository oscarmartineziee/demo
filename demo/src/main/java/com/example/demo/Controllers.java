
package com.example.demo;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
 
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.object.Acta;
 
import com.example.object.ConsejoUsuarios;
import com.example.object.Equipos;
 
import com.example.object.Seccion;
import com.example.object.User;
import com.example.repositories.ActaRepository;
import com.example.repositories.ConsejoUsuarioRepository;
import com.example.repositories.EquipoRepository;
import com.example.repositories.SeccionRepository;
import com.example.repositories.UserRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
 

 @RestController    // This means that this class is a Controller
 
 @RequestMapping(path="/demo") // This means URL's start with /demo (after Application path)
public class Controllers {
	 
	 
	@Autowired // This means to get the bean called userRepository
	           // Which is auto-generated by Spring, we will use it to handle the data	 
	private UserRepository userRepository;
	@Autowired
	private ActaRepository actaRepository;
	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private SeccionRepository seccionRepository;
	@Autowired
	private ConsejoUsuarioRepository cr;
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.POST, path="/add",consumes = "application/json", produces = "application/json") 
	public  ResponseEntity<?> addNewUser (@RequestBody
			String name, @RequestHeader("headerIEE") String header ,UriComponentsBuilder ucBuilder) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		long startTime = System.currentTimeMillis();		 
 
	 
		User n = new User();
		n.setName(name);

	 
	    userRepository.save(n);
	    long endTime = System.currentTimeMillis();
	    //String email="admin@admin.com";
 
	  
	    System.out.println(userRepository.findByName("oscar").toString());
	    //System.out.println(userRepository.findAll().toString());
		System.out.println("That took " + (endTime - startTime) + " milliseconds");
		HttpHeaders headers = new HttpHeaders();
	        headers.setLocation(ucBuilder.path("/api/user/{id}").buildAndExpand(n.getId()).toUri());
	        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/addNewActa",consumes = "application/json", produces = "application/json") 
	public ResponseWS addActa(@RequestHeader("headerIEE") String header ,@Context HttpServletRequest requestContext, @RequestBody final String value) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		Gson s = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
 
		//Response resp = null;
		HeaderIEE headerIEE= s.fromJson(header, HeaderIEE.class);
		User user= headerIEE.getUsuario();
		Acta acta= new Acta();
		
		Seccion datos= new Seccion();
		boolean resultado= false;
		
		ResponseWS valor = new ResponseWS();
		try
		{
			acta= s.fromJson(value, Acta.class);
			byte[] bytes =  java.util.Base64.getDecoder().decode(acta.getBaseByte());
			acta.setImg(bytes);
			datos= seccionRepository.findBySeccion(acta.getSeccion());
			acta.setDistrito(datos.getDistrito2());
			acta.setUser(user);
			actaRepository.save(acta);
			 
			valor.setCodeError(200);
			valor.setData(resultado);
			valor.setMessage("Operacion Exitosa");
			valor.setDescError("");
			
		    return valor;
	
		} 
		catch(Exception j)
		{
			valor.setCode(500);
			valor.setCodeError(500);
			valor.setDescError(j.toString());
			return valor;
		}
	
	 
	}
  
	@CrossOrigin
	@RequestMapping(value = "/imageAndData", method = RequestMethod.GET,  produces = "application/json")
	public @ResponseBody Response  getFileStrinsg()  {
		
		Acta tmp=actaRepository.findTopByOrderByIdDesc(); 
		ResponseWS valor = new ResponseWS();
	return valor.setResponse(200, tmp);
		
	}
	@CrossOrigin
	@RequestMapping(method=RequestMethod.POST, path="/doLogin",consumes = "application/json", produces = "application/json") 
	public ResponseWS doLogin(@RequestHeader("headerIEE") String header ,@Context HttpServletRequest requestContext, @RequestBody final String value) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		 
		ResponseWS valor = new ResponseWS();
		Gson s = new Gson();
		HeaderIEE headerIEE= s.fromJson(header, HeaderIEE.class);
		Equipos equipo= headerIEE.getEquipo();
		ConsejoUsuarios consejoUser= new ConsejoUsuarios();	
		User u= new User();
 
		try
		{
			u= s.fromJson(value, User.class);
			String password=u.getPassword();
			u=userRepository.findByName(u.getName());
			
			if(u!=null)
			{
				valor.setMessage("Done");
				u.setPassword(u.getPassword().replace("$2y$", "$2a$"));
				if(BCrypt.checkpw(password,u.getPassword() )) {
					valor.setMessage("Operacion Exitosa");
					consejoUser= cr.findByUserId(u.getId());
					if(equipo.getConsejo().getId()!=consejoUser.getConsejo().getId())
					{
						u=null;
						
						valor.setMessage("Equipo Asignado No Corresponde al Consejo");
					}else if(!u.getPerfil().getTipo().equals("capturista_catd"))
						{
							u=null;
							valor.setMessage("El usuario no tiene Perfil De Capturista");
						}
				} 
				else 
				{
					u=null;
					valor.setMessage("Credenciales Incorrectas");
				}
			}
			else
				valor.setMessage("Credenciales Incorrectas");
			
			
			valor.setData(u);
			valor.setCode(200);
			valor.setDescError("");
		
		 
			
			   return valor;
		} 
		catch(Exception j)
		{		
			valor.setDescError(j.toString());
			valor.setCode(500);
			valor.setCodeError(500);
			return valor;
		}
	
	 
	}
	
	@CrossOrigin
	@RequestMapping(method=RequestMethod.POST, path="/validaEquipo",consumes = "application/json", produces = "application/json") 
	public ResponseWS validaEquipoIp(@Context HttpServletRequest requestContext, @RequestBody final String value) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		 
		ResponseWS valor = new ResponseWS();
		Gson s = new Gson();
 
		Equipos u= new Equipos();

		try
		{
			u= s.fromJson(value, Equipos.class);
		 
			u=equipoRepository.findByMacAndIp(u.getMac(),u.getIp());
			valor.setMessage("Operacion Exitosa");
			valor.setCode(200);
			if(u==null)
				{
					valor.setMessage("El equipo no se encuentra registrado en la RedPREP");
					valor.setCode(401);
				}
			valor.setData(u);
			valor.setDescError("");
			
		 
			return valor;
		} 
		catch(Exception j)
		{
			valor.setCodeError(500);
			valor.setCode(500);
			valor.setDescError(j.toString());
			return valor;
		}
	
	 
	}
	
 }