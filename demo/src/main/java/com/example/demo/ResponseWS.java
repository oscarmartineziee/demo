 
package com.example.demo;
 

import javax.ws.rs.core.Response;

public class ResponseWS {
	int code;
	int codeError;
	String descError;
	String message;
	Object data;
 
 
	public String getDescError() {
		return descError;
	}
	public void setDescError(String descError) {
		this.descError = descError;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public int getCodeError() {
		return codeError;
	}
	public void setCodeError(int codeError) {
		this.codeError = codeError;
	}
	public Response setResponse(int responseValue, Object entity)
	{
		 
	    return Response
	            .status(responseValue)
	            .entity(entity)
	            .build();
	}
}
